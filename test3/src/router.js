import Vue from "vue"
import VueRouter from 'vue-router'
import ActionSheet from "./views/ActionSheet.vue"
// 全局使用vue-router
Vue.use(VueRouter)

// 暴露vueRouter实例化对象
export default new VueRouter({
	routes:[
		{
			path: '/',
			name: 'hello',
			component:ActionSheet
		},
	]
})