import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import layer from 'layer'
// 全局引入
import Mint from'mint-ui'
import 'mint-ui/lib/style.css'
Vue.use(Mint);
Vue.prototype.$layer = layer;
Vue.config.productionTip = false

new Vue({
  // 这里需要注意的坑 就是只有名字为router是才能简写，
  // 如果是router4，则必须乖乖的改成 router: router4
  // store也是这样的
  router,
  store,
  render: h => h(App),
}).$mount('#app')
